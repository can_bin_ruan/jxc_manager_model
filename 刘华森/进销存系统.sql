
 create database StockManagement

use StockManagement
go
  
    --账户表
  create table AccountInfo(
         AccountInfoId int primary  key identity(1,1), --账户id
		 RealName nvarchar(10) not null, --客户姓名
		 AcodePhone nvarchar(20) not null, --客户电话
		 AccountPhone nvarchar(20) not null,--客户身份证
		 Site nvarchar(20) not null,--客户地址
		 
  );

   insert into AccountInfo(RealName,AcodePhone,AccountPhone,Site)values('李总',123456748911,45042222556551563,'福建省龙岩市新罗区')
   insert into AccountInfo(RealName,AcodePhone,AccountPhone,Site)values('黄总',123456749632,45042222556514208,'福建省厦门市集美区')
   insert into AccountInfo(RealName,AcodePhone,AccountPhone,Site)values('张总',123456747896,45042222556598562,'福建省宁德市蕉城区')
   insert into AccountInfo(RealName,AcodePhone,AccountPhone,Site)values('胡总',123456741259,45042222556578513,'福建省莆田市荔城区')
   go
   --进出货负责部门
  create table Dept(
       DeptId int primary key identity (1,1),  --部门id
	   Director nvarchar(20) not null,  -- 进出货主管
  );
    
	insert into Dept(Director)values('陈主管')
	insert into Dept(Director)values('邹主管')
	GO
  
  -- 供应商 
   create table Suppliers(
         SupplierId int primary key identity(1,1), -- 自增id
		 Name nvarchar(20) not null, --负责人名字
		 Supplier nvarchar(20) not null, -- 供应商 
   );
  go  
    insert into Suppliers(Name,Supplier)values('马总','华东区域供应商')
	insert into Suppliers(Name,Supplier)values('王总','华南区域供应商')

   -- 进货表
   create table Procurement(
         ProcurementId int primary key identity(1,1),  --进货id
		 TypeOfGoods nvarchar(20) not null, --商品名称
		 RealName nvarchar(30) not null,--供应商姓名
		 GoodsMoney nvarchar(100) not null, -- 商品价格
		 QuantityGoode nvarchar(900) not null,--进货数量
		 ProcurementSite nvarchar(30) not null, --进货地址
		 Procurementtime smalldatetime not null, --进货时间

   );
    
	 insert into Procurement(TypeOfGoods,RealName,GoodsMoney,QuantityGoode,ProcurementSite,Procurementtime)
	 values('洗衣液','王总',15,100,'华东区域供应商',GETDATE())
	 insert into Procurement(TypeOfGoods,RealName,GoodsMoney,QuantityGoode,ProcurementSite,Procurementtime)
	 values('洗衣液','马总',15,100,'华南区域供应商',GETDATE())

	
	    --进货合同表
      create table BuyOrder(
	      BuyOrderId int primary key identity(1,1),  -- 进货合同id
		  BuyOrderTypeOfGoods nvarchar(30) not null, --进货的商品合同名称
		  WriteDate smalldatetime not null,  --合同签订时间
		  InsureDate smalldatetime not null, -- 合同生效时间
		  EndDdate  smalldatetime not null, -- 合同到期时间
		  Supplier nvarchar(20) not null,  -- 甲方合同负责人(供应商)
		  Director nvarchar(20) not null,  --乙方合同负责人(部门)
	  );
	   
	    insert into BuyOrder(BuyOrderTypeOfGoods,WriteDate,InsureDate,EndDdate,Supplier,Director)
		values('日常生活用品',GETDATE(),2020-8-12,2025-8-12,'马总','陈总')


	   --  合同表供应商Director外键到部门表的Director
	   alter table BuyOrder add constraint FK_BuyOrder_Dept foreign key (BuyOrderId) references Dept (DeptId)

       --  合同表供应商外键到供应商的Supplier
	   alter table BuyOrder add constraint FK_BuyOrder_Suppliers foreign key(BuyOrderId) references Procurement(ProcurementId)



   --销售表

   create table GoodsSell(
        GoodsSellId int primary key identity (1,1),  --销售id
		RealName nvarchar(20) not null,  --销售客户姓名
		GoodsSellMoney nvarchar(900) not null, --销售价格
		SalesAdd nvarchar(30) not null,--销售地址
		TypeOfGoods nvarchar(20) not null ,--销售商品名称
		GodesSellTime smalldatetime not null , --销售时间
		

   );

   insert into GoodsSell(RealName,GoodsSellMoney,SalesAdd,TypeOfGoods,GodesSellTime)
   values('李总',100,'福建省龙岩市新罗区','洗衣液',GETDATE())
   insert into GoodsSell(RealName,GoodsSellMoney,SalesAdd,TypeOfGoods,GodesSellTime)
   values('黄总',10,'福建省厦门市集美区','口罩',2020-1-5)



    --销售表的TypeOfGood外键到进货表的TypeOfGood
	   
	   alter table GoodsSell add constraint FK_GoodsSell_Procurement foreign key(GoodsSellId) references Procurement(ProcurementId)




    -- 销售合同表
     create table SaleOrder(
	      BuyOrderId int primary key identity(1,1),  -- 销售合同id
		  BuyOrderTypeOfGoods nvarchar(30) not null, --销售的商品合同名称
		  WriteDate smalldatetime not null,  --合同签订时间
		  InsureDate smalldatetime not null, -- 合同生效时间
		  EndDdate  smalldatetime not null, -- 合同到期时间
		  Supplier nvarchar(20) not null,  -- 甲方合同负责人(供应商)
		  Director nvarchar(20) not null,  --乙方合同负责人(部门)
	  );

	  insert into SaleOrder(BuyOrderTypeOfGoods,WriteDate,InsureDate,EndDdate,Supplier,Director)
	  values('电子产品',GETDATE(),2020-11-11,2021-2-1,'王总','邹主管')

	--销售合同表Director外键到部门Director
	alter table SaleOrder add constraint FK_SaleOrder_Dept foreign key (BuyOrderId) references Dept (DeptId)

    -- 销售合同表Supplier外键到供应商表Supplier
	alter table SaleOrder add constraint FK_SaleOrder_Suppliers foreign key (BuyOrderId) references Suppliers(SupplierId)
    
   --入库存表
  
   create table Warehouse(
            WarehouseId int primary key identity (1,1),  --入库存id
			TypesofInventory nvarchar(900) not null,  --库存种类
            TypeOfGoods nvarchar(50) not null ,  -- 库存商品名称
			WarehouseTime smalldatetime not null, --进库时间
			Director nvarchar(20) not null,  -- 入库负责主管部门
			WarehouseAmount nvarchar(1000) not null ,  --库存数量

   );

   insert into Warehouse(TypesofInventory,TypeOfGoods,WarehouseTime,Director,WarehouseAmount)
   values('生活日常用品','洗衣液',2020-1-8,'邹主管',1000)

     -- 入库表Director关联到部门表的Director
		alter table Warehouse add constraint FK_Warehouse_Dept foreign key (WarehouseId) references Dept (DeptId)

   -- 出库表
     create table OutWarehouse(
	      OutWarehouseId int primary key identity (1,1),  --出库id
		  OutTypeOfGoods nvarchar(20) not null, --出库商品名称
		  OutTypesofInventory nvarchar(20) not null, --出库种类
		  Director nvarchar(20) not null, --出库负责主管
		  OutWarehouseTime smalldatetime not null, -- 出库时间
	 );

	  insert into OutWarehouse (OutTypeOfGoods,OutTypesofInventory,Director,OutWarehouseTime)
	   values('手机','电子产品','陈主管',GETDATE())

	 --出库表的Director外键到部门表的Director
	 alter table OutWarehouse add constraint FK_OutWarehouse_Dept foreign key (OutWarehouseId) references Dept(DeptId)
   
    