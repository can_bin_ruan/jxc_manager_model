create database InvoicingDataManagementSystem

go

use InvoicingDataManagementSystem

go

--进货表
create table Purchase(
PurchaseId int identity(1,1) primary key,
ProductID varchar(30) not null unique,				--商品编号
GoodsType varchar(30) not null unique,				--商品类别
CommoditySum int not null,							--商品数量
CommodityTotalPrices money not null,				--总价合计	
PurchaseTime smalldatetime not null,                --进货时间
)


--销售表
create table Sales(
SalesId int identity(1,1) primary key,				
ProductID varchar(30) not null unique,				--商品编号
GoodsType varchar(30) not null,						--商品类别
SalesQuantity int not null,							--销售数量
SalesUnivalence	money not null,						--销售单价
SalesTotalPrices money not null,					--总价合计
SalesTime smalldatetime not null,					--销售时间
EmployeeId nvarchar(20) not null unique,			--销售人员编号
)
alter table Sales add constraint Fk_Sales_Purchase foreign key(ProductID) references Purchase(ProductID)
alter table Sales add constraint Fk_Sales_StaffInformation foreign key(EmployeeId) references StaffInformation(EmployeeId)


--库存表
create table Inventory(
InventoryId int identity(1,1) primary key,				
GoodsType varchar(30) not null unique,				--商品类别
ProductID varchar(30) not null unique,				--商品编号
InventoryQuantity int not null,						--库存数量
WarehouseEntryTime smalldatetime not null,          --入库时间
)
alter table Inventory add constraint Fk_Inventory_Purchase foreign key(ProductID) references Purchase(ProductID)


--商品信息
create table CommodityInformation(
CommodityInformationId int identity(1,1) primary key,
ProductID varchar(30) not null unique,				--商品编号
SupplierNumber nvarchar(30) not null unique,		--供应商编号
ProductIntroduction nvarchar(300),					--商品简介
SalesUnivalence	money not null,						--销售单价
)
alter table CommodityInformation add constraint Fk_CommodityInformation_Purchase foreign key(ProductID) references Purchase(ProductID)
alter table CommodityInformation add constraint Fk_CommodityInformation_Supplier foreign key(SupplierNumber) references Supplier(SupplierNumber)


--供应商信息
create table Supplier(
SupplierId int identity(1,1) primary key,		
SupplierNumber nvarchar(30) not null unique,		--供应商编号
SupplierName nvarchar(30) not null,					--供应商名称
SuppliersSite nvarchar(80),							--供应商地址
ContactNumber varchar(11) not null,					--联系电话
)


--退货信息
create table ReturnInformation(
ReturnInformationId int identity(1,1) primary key,
ProductID varchar(30) not null unique,				--商品编号
ReasonForReturn	nvarchar(200) not null,				--退货原因
RefundThePrice money not null,						--退款价格
PurchaserNumber varchar(11) not null,				--买家联系电话
EmployeeId nvarchar(20) not null,					--售后人员
)
alter table ReturnInformation add constraint Fk_ReturnInformation_Purchase foreign key(ProductID) references Purchase(ProductID)
alter table ReturnInformation add constraint Fk_ReturnInformation_StaffInformation foreign key(EmployeeId) references StaffInformation(EmployeeId)


--员工信息
create table StaffInformation(
StaffInformationId int identity(1,1) primary key,
EmployeeId nvarchar(20) not null unique,				--员工编号
HomeAddress nvarchar(80) not null,					--家庭住址
ContactNumber varchar(11) not null,					--联系电话
CurrentStatus varchar(10) not null,					--当前状态
)

