create database System
use System

--客户表
create table Customer
(
 Customer_id int primary key identity(1,1), --客户Id
 Customer_Name varchar (20),                --客户名称
 Customer_Phone varchar (20),               --客户电话
 Customer_Address varchar (50),             --客户地址
 Customer_Linkman varchar (20),             --联系人
)


--商品表
create table Commodity
(
 Commodity_id int primary key identity(1,1), --商品Id
 Commodity_Name varchar (20),                --商品名称
 Commodity_PurchasingPrice money,            --商品进价
 Commodity_SellingPrice money,               --商品售价
 Commodity_Num int,                          --商品库存
)

--供货商表
create table Supplier
(
 Supplier_Id int primary key identity(1,1), --供货商Id
 Supplier_Name varchar(20),                 --供货商名称
 Supplier_Phone varchar(20),                --供货商电话
 Supplier_Address varchar(50),              --供货商地址
 Supplier_Linkan varchar(20),               --联系人
)

--部门表
create table Department
(
 Department_Id int primary key identity(1,1),--部门Id
 Department_Name varchar(20),                --部门名称
) 

--员工表
create table Employee 
(
 Employee_Id int primary key identity(1,1),  --员工Id
 Employee_Name varchar(20),                  --员工姓名
 Employee_Phone varchar(20),                 --员工电话
 Employee_Address varchar(20),               --员工住址
 Employee_Duty varchar(20),                  --员工职务
 Employee_Department varchar(20),            --员工所属部门
 Employee_BankCard varchar(20),              --员工银行卡
 Employee_IdCard varchar(20),                --员工身份证
)

--采购入库
create table Purchase
(
 Purchase_Id int primary key identity(1,1),  --采购入库编号
 Commodity_id int not null,                  --采购入库商品Id
 Purchase_Num int not null,                  --采购入库商品数量
 Purchase_Price money,                       --采购商品单价
 Purchase_Employee varchar(20),              --采购工作人员
 Purchase_Supplier varchar(20),              --供货商
 Purchase_Time smalldatetime,                --采购时间
)

--销售出库
create table Sales 
(
 Sales_Id int primary key identity(1,1),    --销售出库编号
 Commodity_id int not null,                 --销售出库商品Id
 Sales_Num int not null,                    --销售出库商品数量
 Sales_Price money,                         --销售出库商品单价
 Sales_Employee varchar(20),                --销售出库工作人员
 Sales_Customer varchar(20),                --购买客户
 Sales_Time smalldatetime,                  --销售时间
)

--退货
create table SalesReturn
(
 Return_Id int primary key identity(1,1),  --退货编号
 Commodity_id int not null,                --退货商品Id       
 Return_Num int not null,                  --退货商品数量
 Return_Person varchar(20),                --退货人
 Return_Reason varchar(100),               --退货原因
 Return_Time smalldatetime,                --退货时间
)

--仓库表
create table Warehouse
(
 Warehouse_id int primary key identity(1,1),--仓库编号
 Commodity_id int not null,                 --商品编号
 Inventory int not null,                    --库存数量    
)